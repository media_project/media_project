"""media_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path


admin.site.site_header = 'Media project'
admin.site.site_title = 'Media project'

urlpatterns = [
    re_path(r'^', include('apps.central.urls', namespace='central')),
    path('admin/', admin.site.urls),
    path('auth/', include('apps.myauth.urls', namespace='myauth')),
    path('digests/', include('apps.digests.urls', namespace='digests')),
    path('vk_monitoring/', include('apps.vk_monitoring.urls', namespace='vk_monitoring')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
