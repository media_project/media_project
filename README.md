# Media project

### Database configuration
```
CREATE USER example WITH LOGIN PASSWORD 'example';
CREATE DATABASE example_DB WITH OWNER 'example';
```

### Project setup
```
python manage.py migrate
python manage.py collectstatic

# Redis
redis-server

# Celery
celery worker -A conf --loglevel=debug --concurrency=4

# Windows version
celery -A <module> worker -l info -P eventlet

python manage.py runserver
or
python cherry.py
```
