from django.shortcuts import render, redirect, reverse
from django.contrib.auth import authenticate, login
from django.views import View


class LoginView(View):

    def get(self, request):
        return render(request, 'auth/login.html', {})

    def post(self, request):
        data = request.POST
        user = authenticate(username=data['username'], password=data['password'])
        if user:
            login(request, user)
            return redirect(reverse('central:index'))
        else:
            return render(request, 'auth/login.html', {})
