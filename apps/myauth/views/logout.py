from django.contrib.auth import logout
from django.shortcuts import redirect, reverse
from django.views import View


class LogoutView(View):
    """Logout view"""

    def get(self, request):
        """GET"""
        logout(request)
        return redirect(reverse('central:index'))
