from django.shortcuts import render, redirect, reverse
from django.contrib.auth import login
from django.views import View
from django.contrib.auth.models import User


class RegisterView(View):
    """Register view"""

    def get(self, request):
        """GET"""
        return render(request, 'auth/register.html')

    def post(self, request):
        """POST"""
        data = request.POST
        error = self.validation(data)
        if not error:
            user, created = User.objects.get_or_create(username=data.get('username'))
            if created:
                user.set_password(data.get('password'))
                user.save()
                login(request, user)
                return redirect(reverse('central:index'))
            else:
                return render(request, 'auth/register.html', {'error': 'Пользователь уже существует'})
        else:
            return render(request, 'auth/register.html', {'error': error})

    @staticmethod
    def validation(data):
        """Fields validation"""
        if not (data.get('username') or data.get('password') or data.get('password1')):
            return "Заполните все поля"
        if data.get('password') != data.get('password1'):
            return 'Пароли не совпадают'
