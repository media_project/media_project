"""Views"""

from .login import LoginView
from .logout import LogoutView
from .register import RegisterView
