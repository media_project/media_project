from django.shortcuts import render
from django.views import View
from gensim.summarization.summarizer import summarize


class Index2View(View):
    def get(self, request):
        return render(request, 'digests/index2.html')

    def post(self, request):
        data = request.POST
        text = summarize(data['text'], ratio=float(data['n']))
        return render(request, 'digests/index2.html', {'text': text})
