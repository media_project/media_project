from itertools import combinations

import networkx as nx
from django.shortcuts import render
from django.views import View
# from nltk import download
from nltk.tokenize import sent_tokenize, RegexpTokenizer
from nltk.stem.snowball import RussianStemmer


class IndexView(View):
    def get(self, request):
        # download('punkt')
        return render(request, 'digests/index.html')

    def post(self, request):
        data = request.POST
        text = self.extract(data['text'], int(data['n']))
        return render(request, 'digests/index.html', {'text': text})

    @staticmethod
    def similarity(s1, s2):
        if not len(s1) or not len(s2):
            return 0.0
        return len(s1.intersection(s2)) / (1.0 * (len(s1) + len(s2)))

    def text_rank(self, text):
        sentences = sent_tokenize(text)
        tokenizer = RegexpTokenizer(r'\w+')
        lmtzr = RussianStemmer()
        words = [set(lmtzr.stem(word) for word in tokenizer.tokenize(sentence.lower()))
                 for sentence in sentences]

        pairs = combinations(range(len(sentences)), 2)
        scores = [(i, j, self.similarity(words[i], words[j])) for i, j in pairs]
        scores = filter(lambda x: x[2], scores)

        g = nx.Graph()
        g.add_weighted_edges_from(scores)
        pr = nx.pagerank(g)

        return sorted(((i, pr[i], s) for i, s in enumerate(sentences) if i in pr), key=lambda x: pr[x[0]], reverse=True)

    def extract(self, text, n=3):
        tr = self.text_rank(text)
        top_n = sorted(tr[:n])
        return ' '.join(x[2] for x in top_n)
