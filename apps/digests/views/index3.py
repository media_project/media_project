from django.views import View
from django.shortcuts import render
from newspaper import Article


class Index3View(View):
    """Index page 3th edition"""

    def get(self, request):
        """Get"""
        return render(request, 'digests/index3.html', {})

    def post(self, request):
        """Post"""
        data = request.POST
        url = data.get('url')
        # url = 'https://rb.ru/longread/in-the-end/'
        article = Article(url)

        article.download()
        article.parse()
        article.nlp()
        summary = article.summary

        return render(request, 'digests/index3.html', {'text': summary})
