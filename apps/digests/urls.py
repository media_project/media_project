from django.urls import re_path

from .views import IndexView, Index2View, Index3View


app_name = 'digests'
urlpatterns = (
    re_path(r'^1/$', IndexView.as_view(), name='index'),
    re_path(r'^2/$', Index2View.as_view(), name='index2'),
    re_path(r'^3/$', Index3View.as_view(), name='index3')
)
