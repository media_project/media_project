# Generated by Django 2.1.5 on 2019-02-12 01:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vk_monitoring', '0003_auto_20190207_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='user_id',
            field=models.PositiveIntegerField(blank=True, null=True, unique=True, verbose_name='id пользователя'),
        ),
    ]
