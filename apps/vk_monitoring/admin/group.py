from django.contrib import admin

from ..tasks import person_task


class GroupAdmin(admin.ModelAdmin):

    def get_group_members(self, obj, groups):
        for group in groups:
            person_task.delay(group_id=group.group_id)

    get_group_members.short_description = 'Начать парсинг'

    list_per_page = 20
    actions = (get_group_members,)
