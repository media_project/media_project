from django.contrib import admin

from ..models import Post


class PostAdmin(admin.ModelAdmin):
    """Post admin"""

    def user_id(self, obj):
        return obj.user

    list_per_page = 20
    list_display = ('user_id', 'link', 'likes', 'views')

    class Meta:
        model = Post
