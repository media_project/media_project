from django.contrib import admin

from ..tasks import post_task
from ..models import Person


class PersonAdmin(admin.ModelAdmin):
    """Person admin"""

    def person_parser(self, request, persons):
        for person in persons:
            post_task.delay(person_id=person.user_id)

    person_parser.short_description = 'Начать парсинг'

    def status_(self, obj):
        return 'error' if obj.status == Person.STATUS_CHOICE_ERROR else '-'

    list_filter = ('status',)
    search_fields = ('user_id',)
    actions = (person_parser,)
    list_display = ('user_id', 'status_')
    list_per_page = 20
