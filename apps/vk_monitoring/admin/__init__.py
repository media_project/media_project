from django.contrib import admin

from .person import PersonAdmin
from .post import PostAdmin
from .group import GroupAdmin
from ..models import (
    Person,
    Group,
    Post,
    NewsResource
)


admin.site.register(Person, PersonAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(NewsResource)
