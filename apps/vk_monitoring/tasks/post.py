import pydash
from celery import shared_task
from envparse import env

from ..models import (
    NewsResource,
    Post,
    Person
)
from ..parsers import PersonParser
from ..parsers.post import AccountDeletedException


@shared_task
def post_task(person_id):
    """Post task"""
    service_key = env('SERVICE_KEY', cast=str, default='')
    person_parser = PersonParser(service_key=service_key)

    try:
        posts = person_parser.get_posts(user_id=person_id)

        for posts_ in posts:
            for post in posts_:
                post_obj = person_parser.get_post(post)
                print(post_obj)
                if post_obj.get('views') is None:
                    post_obj['views'] = 1
                Post.objects.update_or_create(
                    user=Person.objects.get_or_create(user_id=person_id),
                    text=post_obj.get('text', ''),
                    link=post_obj.get('link', ''),
                    views=pydash.get(post_obj, 'views', 0),
                    likes=post_obj.get('likes', 0),
                )
    except AccountDeletedException:
        print('catch you!')
        person, created = Person.objects.get_or_create(user_id=person_id)
        print(person, created)
        person.status = Person.STATUS_CHOICE_ERROR
        person.save()
        return
