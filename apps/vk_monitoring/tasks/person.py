from time import sleep

from celery import shared_task
from envparse import env

from ..models import Person
from ..parsers import GroupParser


env.read_envfile()


@shared_task
def person_task(group_id='kfu_ist'):
    service_key = env('SERVICE_KEY', cast=str, default='')
    group_parser = GroupParser(service_key=service_key)
    members_list = group_parser.get_members(group_id=group_id)

    for member_sub_list in members_list:
        [Person.objects.get_or_create(user_id=member, defaults={'status': Person.STATUS_CHOICE_NEW})
         for member in member_sub_list]
