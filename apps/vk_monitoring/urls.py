from django.urls import path

from .views import IndexView


app_name = 'vk_monitoring'
urlpatterns = (
    path('', IndexView.as_view(), name='index'),
)
