"""
    Returns lists of group members ids
"""
from time import sleep
from math import ceil

import vk_requests


class GroupParser:
    def __init__(self, service_key=None):
        self.api = self.get_api(service_key)

    @staticmethod
    def get_api(service_key):
        api = vk_requests.create_api(service_token=service_key)
        return api

    def get_members(self, group_id, offset=1000, pause=20):
        count = offset
        user = self.api.groups.getMembers(group_id=group_id, count=1, offset=1)
        count_ = user.get('count')
        sleep(5)

        for index in range(ceil(count_/offset)):
            users = self.api.groups.getMembers(group_id=group_id, count=count, offset=index*offset)
            yield users.get('items', [])
            sleep(pause)
