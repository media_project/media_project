"""
    Returns posts of user
"""
from math import ceil
from time import sleep

import pydash
import vk_requests
from vk_requests.exceptions import VkAPIError


class AccountDeletedException(Exception):
    """Account was deleted or banned"""


class PersonParser:
    def __init__(self, service_key=None):
        self.api = self.get_api(service_key)

    @staticmethod
    def get_api(service_key):
        api = vk_requests.create_api(service_token=service_key)
        return api

    def get_posts(self, user_id, offset=1000, pause=20):
        count = offset
        try:
            posts = self.api.wall.get(owner_id=user_id, count=1, offset=1)
        except VkAPIError as err:
            if err.code == 18:
                raise AccountDeletedException('Banned or locked')
            else:
                print(str(err))
            return []

        count_ = posts.get('count')
        sleep(5)

        for index in range(ceil(count_/offset)):
            posts = self.api.wall.get(owner_id=user_id, count=count, offset=index*offset)
            yield posts.get('items', [])
            sleep(pause)

    @staticmethod
    def get_post(post):
        news_resource_link = pydash.get(post, 'copy_history.0.attachments.0.link.url', '')

        from_id = pydash.get(post, 'copy_history.0.from_id', '')
        text = pydash.get(post, 'copy_history.0.text', '')
        owner_id = pydash.get(post, 'from_id', '')
        views = pydash.get(post, 'views.count')
        likes = pydash.get(post, 'likes.count')

        group, _ = from_id, True

        post_obj = {
            'text': text,
            'link': "https://vk.com/id{owner_id}?w=wall{owner_id}_{post_id}".format(
                owner_id=owner_id, post_id=post.get('id')),
            'views': views,
            'likes': likes,
            'news_resource_link': news_resource_link
        }
        return post_obj
