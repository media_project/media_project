from django.shortcuts import render
from django.views import View

from ..models import (
    Post,
    Person,
    Group,
    NewsResource,
)
from ..tasks import person_task


class IndexView(View):
    """Index vk view"""

    def get(self, request):
        """GET"""
        groups_cnt = Group.objects.all().count()
        persons_cnt = Person.objects.all().count()
        posts_cnt = Post.objects.all().count()
        resources_cnt = NewsResource.objects.count()
        return render(request, 'vk_monitoring/index.html', locals())

    def post(self, request):
        """POST"""
        # person = Person.objects.last()
        person_task()

        groups_cnt = Group.objects.all().count()
        persons_cnt = Person.objects.all().count()
        posts_cnt = Post.objects.all().count()
        resources_cnt = NewsResource.objects.count()

        return render(request, 'vk_monitoring/index.html', locals())
