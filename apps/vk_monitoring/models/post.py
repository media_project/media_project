from django.db import models


class Post(models.Model):
    """Post model"""

    text = models.TextField(verbose_name='Текст поста', blank=True, null=True)
    group = models.ForeignKey(verbose_name='Группа', to='Group', on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(verbose_name='Пользователь', to='Person', on_delete=models.CASCADE, blank=True, null=True)
    link = models.URLField(verbose_name='Ссылка на пост')
    views = models.PositiveIntegerField(verbose_name='Кол-во просмотров')
    likes = models.PositiveIntegerField(verbose_name='Кол-во лайков')

    objects = models.Manager()

    class Meta:
        """Meta"""

        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
