from django.db import models


class Group(models.Model):
    """Vk group model"""

    name = models.CharField(verbose_name='Название группы', max_length=256)
    group_id = models.CharField(max_length=128, verbose_name='Ссылка на группу', default='')

    objects = models.Manager()

    class Meta:
        """Meta"""

        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'

    def __str__(self):
        return self.name
