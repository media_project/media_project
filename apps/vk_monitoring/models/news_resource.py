from django.db import models


class NewsResource(models.Model):
    """New resources model"""

    url = models.URLField(verbose_name='Ссылка на реурс')

    objects = models.Manager()

    class Meta:
        """Meta"""

        verbose_name = 'Новостной ресурс'
        verbose_name_plural = 'Новостные ресурсы'

    def __str__(self):
        return str(self.url)
