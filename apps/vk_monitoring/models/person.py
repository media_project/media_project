from django.db import models


class Person(models.Model):
    """Vk user model"""

    STATUS_CHOICE_NEW = 0
    STATUS_CHOICE_PROGRESS = 1
    STATUS_CHOICE_DONE = 2
    STATUS_CHOICE_ERROR = 3

    STATUS_CHOICES = (
        (STATUS_CHOICE_NEW, 'Новый'),
        (STATUS_CHOICE_PROGRESS, 'В обработке'),
        (STATUS_CHOICE_DONE, 'Закончен'),
        (STATUS_CHOICE_ERROR, 'Ошибка')
    )

    status = models.CharField(verbose_name='Статус', max_length=64, choices=STATUS_CHOICES, default=STATUS_CHOICE_NEW)
    first_name = models.CharField(verbose_name='Имя', max_length=20, blank=True, null=True)
    last_name = models.CharField(verbose_name='Фамилия', max_length=20, blank=True, null=True)
    link = models.CharField(verbose_name='Ссылка на профиль', max_length=30, blank=True, null=True)
    user_id = models.PositiveIntegerField(verbose_name='id пользователя', blank=True, null=True, unique=True)

    objects = models.Manager()

    class Meta:
        """Meta"""

        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return str(self.user_id)

    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)
