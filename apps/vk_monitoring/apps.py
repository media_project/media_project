from django.apps import AppConfig


class VkMonitoringConfig(AppConfig):
    name = 'vk_monitoring'
