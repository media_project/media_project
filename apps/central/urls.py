from django.urls import re_path
from django.views.generic import TemplateView


app_name = 'central'
urlpatterns = [
    re_path(r'^$', TemplateView.as_view(template_name='central/index.html'), name='index'),
]
